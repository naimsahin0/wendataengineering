import threading


class HotelDataConsumer(threading.Thread):
    def __init__(self, data_queue, dead_letter_queue):
        super().__init__()
        self.data_queue = data_queue
        self.dead_letter_queue = dead_letter_queue

    def process(self, data):
        raise NotImplementedError()
