from package.service.consumer.base import HotelDataConsumer
from package.validate.exception import CustomError
import time


class ExceptionConsumer(HotelDataConsumer):
    def __init__(self, data_queue, dead_letter_queue, db):
        super().__init__(data_queue, dead_letter_queue)
        self.db = db

    def run(self):
        self.process()

    def process(self):
        while True:
            try:
                if not self.dead_letter_queue.is_empty():
                    record = self.dead_letter_queue.dequeue()
                    self.insert_data(record)
                else:
                    time.sleep(1)
            except CustomError as e:
                self.dead_letter_queue.enqueue(e.to_dict())
            except Exception as e:
                self.dead_letter_queue.enqueue({
                    'message': str(e),
                    'data': record,
                    'class_name': self.__class__.__name__,
                    'method_name': self.process.__name__
                })
            finally:
                time.sleep(1)


    def insert_data(self, record):
        try:
            db_instance = self.db.instance()
            db_instance.autocommit = False


            with db_instance.cursor() as cur:
                cur.execute("""
                            INSERT INTO public.Exception (Message, Record, ClassName, MethodName, AdditionalInfo)
                            VALUES (%s, %s, %s, %s, %s);
                            """, (
                record['message'], str(record['data']), record['class_name'], record['method_name'], str({})))

                db_instance.commit()

        except Exception as e:
            db_instance.rollback()
            raise CustomError(
                message=str(e),
                data=record,
                class_name=self.__class__.__name__,
                method_name=self.insert_data.__name__
            )

        finally:
            db_instance.close()
