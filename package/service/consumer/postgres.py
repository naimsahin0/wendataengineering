from package.service.consumer.base import HotelDataConsumer
from package.validate.exception import CustomError
import time


class PostgresConsumer(HotelDataConsumer):
    def __init__(self, data_queue, dead_letter_queue, db):
        super().__init__(data_queue, dead_letter_queue)
        self.db = db

    def run(self):
        self.process()

    def process(self):
        while True:
            try:
                if not self.data_queue.is_empty():
                    record = self.data_queue.dequeue()
                    self.insert_data(record)
                else:
                    time.sleep(1)
            except CustomError as e:
                self.dead_letter_queue.enqueue(e.to_dict())
            except Exception as e:
                self.dead_letter_queue.enqueue({
                    'message': str(e),
                    'data': record,
                    'class_name': self.__class__.__name__,
                    'method_name': self.process.__name__
                })

    def insert_data(self, record):
        try:
            db_instance = self.db.instance()
            db_instance.autocommit = False

            category = record['category']
            chain = record['chain']
            hotel = record['hotel']
            with db_instance.cursor() as cur:
                cur.execute("""
                        INSERT INTO public.Category (CategoryId, CategoryName)
                        VALUES (%s, %s)
                        ON CONFLICT (CategoryId) DO UPDATE SET
                            CategoryName = EXCLUDED.CategoryName;
                        """, (category['CategoryId'], category['CategoryName']))

                cur.execute("""
                        INSERT INTO public.Chain (ChainId, ChainName)
                        VALUES (%s, %s)
                        ON CONFLICT (ChainId) DO UPDATE SET
                            ChainName = EXCLUDED.ChainName;
                        """, (chain['ChainId'], chain['ChainName']))

                cur.execute("""
                        INSERT INTO public.Hotel (HotelId, HotelName, CategoryId, ChainId, Latitude, Longitude)
                        VALUES (%s, %s, %s, %s, %s, %s)
                        ON CONFLICT (HotelId) DO UPDATE SET
                            HotelName = EXCLUDED.HotelName,
                            CategoryId = EXCLUDED.CategoryId,
                            ChainId = EXCLUDED.ChainId,
                            Latitude = EXCLUDED.Latitude,
                            Longitude = EXCLUDED.Longitude;
                        """, (hotel['HotelId'], hotel['HotelName'], hotel['CategoryId'], hotel['ChainId'],
                              hotel['Latitude'], hotel['Longitude']))

                db_instance.commit()

        except Exception as e:
            db_instance.rollback()
            raise CustomError(
                message=str(e),
                data=record,
                class_name=self.__class__.__name__,
                method_name=self.insert_data.__name__
            )

        finally:
            db_instance.close()
