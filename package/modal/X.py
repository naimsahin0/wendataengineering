from pydantic import BaseModel, validator
from typing import Optional


class XCoordinatesModel(BaseModel):
    latitude: float
    longitude: float


class XLocationModel(BaseModel):
    coordinates: XCoordinatesModel
    obfuscation_required: bool
    obfuscated_coordinates: Optional[XCoordinatesModel] = None

    @validator('obfuscated_coordinates', always=True)
    def check_obfuscation(cls, v, values):
        if values['obfuscation_required'] and v is None:
            raise ValueError("Obfuscation required but 'obfuscated_coordinates' is missing.")
        return v


class XCategoryModel(BaseModel):
    id: int
    name: str


class XChainModel(BaseModel):
    id: int
    name: str


class XHotelModel(BaseModel):
    property_id: int
    category: XCategoryModel
    chain: XChainModel
    location: XLocationModel
    name: str
