from package.validate.base import HotelDataValidator
from package.validate.exception import CustomError
from package.modal.X import XHotelModel


class XValidator(HotelDataValidator):
    """
        A validator class that checks if the hotel data conforms to the expected data structure using a Pydantic model.

        This class extends HotelDataValidator to implement specific validation logic for checking the
        integrity and structure of hotel data records.

        Methods:
            check(record): Validates a single hotel data record.
            check_data_structure(record): Validates the data structure of a record against the XHotelModel Pydantic model.
    """
    def check(self, record):
        """
                Validates the given hotel data record. Raises a CustomError if validation fails.

                Args:
                    record (dict): The hotel data record to validate.

                Raises:
                    CustomError: If the record fails to validate, encapsulating the error message, data, class name, and method name.
        """
        try:
            self.check_data_structure(record)
        except Exception as e:
            raise CustomError(
                message=str(e),
                data=record,
                class_name=self.__class__.__name__,
                method_name=self.check.__name__
            )

    def check_data_structure(self, record):
        """
                Checks if the provided record matches the expected data structure defined by the XHotelModel.

                Args:
                    record (dict): The hotel data record to validate.

                Returns:
                    XHotelModel: The validated hotel data record as a Pydantic model instance, confirming it adheres to the model's schema.

                Raises:
                    ValidationError: If the record does not conform to the XHotelModel's schema.
        """
        record = XHotelModel(**record)

        return record
