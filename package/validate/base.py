from abc import ABC, abstractmethod


class HotelDataValidator(ABC):
    @abstractmethod
    def check(self, record):
        pass
