class CustomError(Exception):
    """
    A custom exception class that captures additional information about the error.

    This class extends the base Exception class by adding more context to the errors,
    including data involved in the error, the class name, and the method name where the
    error occurred. This extra information can be very helpful for debugging.

    Attributes:
        message (str): A clear, concise description of the error.
        data (Any): The data related to the error, which might have contributed to the error occurrence.
        class_name (str): The name of the class where the error occurred.
        method_name (str): The name of the method where the error occurred.
    """

    def __init__(self, message, data, class_name, method_name):
        """
        Initializes a new instance of the CustomError with the specified error details.

        Args:
            message (str): The message describing what went wrong.
            data (Any): The data involved when the error happened.
            class_name (str): The name of the class where the error was caught.
            method_name (str): The method name where the error was caught.
        """
        super().__init__(message)
        self.data = data
        self.class_name = class_name
        self.method_name = method_name

    def to_dict(self):
        """
        Converts the error details into a dictionary format, which can be especially useful
        for logging errors in structured logging systems or returning error details in APIs.

        Returns:
            dict: A dictionary containing detailed error information, including the message,
            data, class name, and method name.
        """
        return {
            'message': self.args[0],
            'data': self.data,
            'class_name': self.class_name,
            'method_name': self.method_name
        }
