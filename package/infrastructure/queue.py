import queue
from typing import Any


class CustomQueue:
    """
    A simple class that abstracts Python's built-in queue.Queue operations,
    providing a more intuitive interface for enqueueing and dequeueing items,
    checking if the queue is empty, and getting the queue's size.

    Attributes:
        _queue (queue.Queue): The internal queue object that holds the items.
    """

    def __init__(self):
        """Initializes a new instance of SimpleQueue with an empty queue."""
        self._queue = queue.Queue()

    def enqueue(self, item: Any):
        """
        Adds an item to the end of the queue.

        Args:
            item (Any): The item to be added to the queue.
        """
        self._queue.put(item)

    def dequeue(self) -> Any:
        """
        Removes and returns the item from the front of the queue. This method blocks
        if the queue is empty until an item is available to dequeue.

        Returns:
            Any: The item removed from the front of the queue.
        """
        return self._queue.get()

    def is_empty(self) -> bool:
        """
        Checks whether the queue is empty or not.

        Returns:
            bool: True if the queue is empty, False otherwise.
        """
        return self._queue.empty()

    def size(self) -> int:
        """
        Returns the number of items in the queue.

        Returns:
            int: The current size of the queue.
        """
        return self._queue.qsize()
