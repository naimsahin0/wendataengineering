import psycopg2
from typing import List, Dict, Any
from package.validate.exception import CustomError


class PostgresDatabase:
    """
    A class to manage PostgreSQL database operations with completely isolated transactions,
    suitable for parallel processing environments by ensuring each operation manages its
    own database connection.

    Attributes:
        db_params (dict): Parameters to use for the database connection.
    """

    def __init__(self, db_params: dict):
        """
        Initialize a new PostgresDatabase instance with specific connection parameters.

        Args:
            db_params (dict): Database connection parameters including dbname, user,
                              password, host, and optionally port.
        """
        self.db_params = db_params

    def instance(self):
        return psycopg2.connect(**self.db_params)
