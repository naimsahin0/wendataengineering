import threading
from abc import abstractmethod


class HotelDataProvider(threading.Thread):
    def __init__(self, data_queue, dead_letter_queue, adapter, validator):
        super().__init__()
        self.data_queue = data_queue
        self.dead_letter_queue = dead_letter_queue
        self.adapter = adapter
        self.validator = validator

    @abstractmethod
    def provide_data(self):
        pass
