from package.provider.base import HotelDataProvider


class YProvider(HotelDataProvider):
    def provide_data(self):
        raise NotImplementedError()