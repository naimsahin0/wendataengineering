from package.provider.base import HotelDataProvider
import json
from package.validate.exception import CustomError


class XProvider(HotelDataProvider):
    """
    A data provider class that reads hotel data from a JSON file and processes it
    through an adapter and validator before putting it into a data queue for further processing.
    Any errors encountered during data processing are put into a dead letter queue.

    Attributes:
        filepath (str): The path to the JSON file containing the data.
        data_queue (queue.Queue): A queue where processed records are placed.
        dead_letter_queue (queue.Queue): A queue for storing information about records that failed processing.
        adapter (HotelDataAdapter): An adapter to convert raw data into a structured format.
        validator (Validator): A validator to check the integrity of the data before processing.
    """

    def __init__(self, filepath, data_queue, dead_letter_queue, adapter, validator):
        """
        Initializes the XProvider with the necessary attributes and file path.

        Args:
            filepath (str): The path to the JSON file containing hotel data.
            data_queue (queue.Queue): The queue to which processed data will be added.
            dead_letter_queue (queue.Queue): The queue to which failed records will be added.
            adapter (HotelDataAdapter): Adapter instance to convert raw data.
            validator (Validator): Validator instance to validate data.
        """
        super().__init__(data_queue, dead_letter_queue, adapter, validator)
        self.filepath = filepath

    def run(self):
        """
        Start the provider's operation. This is typically called when the provider is run in a separate thread.
        """
        self.provide_data()

    def provide_data(self):
        """
        Reads data from the specified JSON file and processes each record.
        Records that pass validation are adapted and enqueued in the data queue.
        Records that fail validation or adaptation are enqueued in the dead letter queue.
        """
        with open(self.filepath, 'r') as file:
            data = json.load(file)
            for _, record in data.items():
                try:
                    self.validator.check(record)
                    adapted_record = self.adapter.adapt(record)
                    self.data_queue.enqueue(adapted_record)
                except CustomError as e:
                    self.dead_letter_queue.enqueue(e.to_dict())
                except Exception as e:
                    self.dead_letter_queue.enqueue({
                        'message': str(e),
                        'data': record,
                        'class_name': self.__class__.__name__,
                        'method_name': self.provide_data.__name__
                    })