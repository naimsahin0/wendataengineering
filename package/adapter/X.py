from package.adapter.base import HotelDataAdapter
from package.validate.exception import CustomError


class XAdapter(HotelDataAdapter):
    """
    Adapter class to convert raw hotel data into a structured format suitable for processing or storage.
    Inherits from HotelDataAdapter and implements the adaptation logic specific to the data structure and business rules.

    Methods:
        adapt(record): Converts a raw data record into a structured dictionary with separate entries for hotel, category, and chain.
        rule_obfuscated_coordinates(record): Determines the correct coordinates based on the obfuscation requirement.
    """

    def adapt(self, record):
        """
        Adapt a single raw data record into structured hotel data.

        Args:
            record (dict): A dictionary containing raw hotel data including property details, category, chain, and location.

        Returns:
            dict: A dictionary with structured data for category, chain, and hotel including obfuscated or regular coordinates.

        Raises:
            CustomError: If an error occurs during the data adaptation process.
        """
        try:
            latitude, longitude = self.rule_obfuscated_coordinates(record)

            category = {
                'CategoryId': record['category']['id'],
                'CategoryName': record['category']['name']
            }
            chain = {
                'ChainId': record['chain']['id'],
                'ChainName': record['chain']['name']
            }
            hotel = {
                'HotelId': record['property_id'],
                'HotelName': record['name'],
                'CategoryId': record['category']['id'],
                'ChainId': record['chain']['id'],
                'Latitude': latitude,
                'Longitude': longitude
            }

            return {'category': category, 'chain': chain, 'hotel': hotel}

        except Exception as e:
            raise CustomError(
                message=str(e),
                data=record,
                class_name=self.__class__.__name__,
                method_name=self.adapt.__name__
            )

    def rule_obfuscated_coordinates(self, record):
        """
        Determines the latitude and longitude for a hotel based on its obfuscation requirements.

        Args:
            record (dict): A dictionary containing the location details of a hotel, including whether obfuscation is required.

        Returns:
            tuple: A tuple containing the latitude and longitude of the hotel.
        """
        if record['location'].get('obfuscation_required', False):
            latitude = record['location']['obfuscated_coordinates']['latitude']
            longitude = record['location']['obfuscated_coordinates']['longitude']
        else:
            latitude = record['location']['coordinates']['latitude']
            longitude = record['location']['coordinates']['longitude']

        return latitude, longitude
