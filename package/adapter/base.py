from abc import ABC, abstractmethod


class HotelDataAdapter(ABC):
    @abstractmethod
    def adapt(self, record):
        pass
