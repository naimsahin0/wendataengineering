from package.adapter.base import HotelDataAdapter


class YAdapter(HotelDataAdapter):
    def adapt(self, record):
        raise NotImplementedError()
