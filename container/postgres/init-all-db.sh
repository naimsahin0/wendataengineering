#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "postgres" <<-EOSQL
        CREATE DATABASE hotel;
        GRANT ALL PRIVILEGES ON DATABASE hotel TO postgres;
;
EOSQL

psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "hotel" <<-EOSQL
        create schema if not exists public authorization postgres;
        CREATE TABLE public.Category (CategoryId INT PRIMARY KEY, CategoryName VARCHAR(255) NOT NULL);
        CREATE TABLE public.Chain (ChainId INT PRIMARY KEY,ChainName VARCHAR(255) NOT NULL);
        CREATE TABLE public.Hotel (HotelId INT PRIMARY KEY,HotelName VARCHAR(255) NOT NULL,CategoryId INT,ChainId INT,Latitude DECIMAL(9,6),Longitude DECIMAL(9,6),FOREIGN KEY (CategoryId) REFERENCES public.Category(CategoryId),FOREIGN KEY (ChainId) REFERENCES public.Chain(ChainId));
        CREATE TABLE public.Exception (id SERIAL PRIMARY KEY,CreatedAt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,Message TEXT,Record TEXT,ClassName VARCHAR(255),MethodName VARCHAR(255),AdditionalInfo JSONB);
;
EOSQL