from package.infrastructure.queue import CustomQueue
from package.infrastructure.postgres import PostgresDatabase
from package.validate.X import XValidator
from package.adapter.X import XAdapter
from package.provider.X import XProvider
from package.service.consumer.postgres import PostgresConsumer
from package.service.consumer.exception import ExceptionConsumer

import os
db_params = {
    'dbname': os.environ['DB_NAME'],
    'user': os.environ['DB_USER'],
    'password': os.environ['DB_PASS'],
    'host': os.environ['DB_HOST']
}

postgres_database = PostgresDatabase(db_params=db_params)

data_queue = CustomQueue()
dead_letter_queue = CustomQueue()

x_validator = XValidator()
x_adapter = XAdapter()
x_provider = XProvider(
    filepath='data/xprovider.json',
    data_queue=data_queue,
    dead_letter_queue=dead_letter_queue,
    adapter=x_adapter,
    validator=x_validator
)

postgres_consumer = PostgresConsumer(
    data_queue=data_queue,
    dead_letter_queue=dead_letter_queue,
    db=postgres_database
)
exception_consumer = ExceptionConsumer(
    data_queue=data_queue,
    dead_letter_queue=dead_letter_queue,
    db=postgres_database
)


x_provider.start()
postgres_consumer.start()
exception_consumer.start()

x_provider.join()
postgres_consumer.join()
exception_consumer.join()

