import unittest
from package.adapter import X

class TestXAdapter(unittest.TestCase):

    def setUp(self):
        self.xadapter = X.XAdapter()

    def test_adapt(self):
        test_record = {
            'property_id': '1',
            'name': 'test hotel',
            'category': {'id': 'cat1', 'name': 'category1'},
            'chain': {'id': 'chn1', 'name': 'chain1'},
            'location': {
                'coordinates': {'latitude': '123', 'longitude': '456'},
                'obfuscation_required': True,
                'obfuscated_coordinates': {'latitude': '789', 'longitude': '012'}
            }
        }

        expected_output = {
            'category': {
                'CategoryId': 'cat1',
                'CategoryName': 'category1'
            },
            'chain': {
                'ChainId': 'chn1',
                'ChainName': 'chain1'
            },
            'hotel': {
                'HotelId': '1',
                'HotelName': 'test hotel',
                'CategoryId': 'cat1',
                'ChainId': 'chn1',
                'Latitude': '789',
                'Longitude': '012'
            }
        }

        self.assertEqual(expected_output, self.xadapter.adapt(test_record))

    def test_rule_obfuscated_coordinates(self):
        test_record_obfuscated = {
            'location': {
                'coordinates': {'latitude': '123', 'longitude': '456'},
                'obfuscation_required': True,
                'obfuscated_coordinates': {'latitude': '789', 'longitude': '012'}
            }
        }
        test_record = {
            'location': {
                'coordinates': {'latitude': '123', 'longitude': '456'},
                'obfuscation_required': False
            }
        }

        self.assertEqual(('789', '012'), self.xadapter.rule_obfuscated_coordinates(test_record_obfuscated))
        self.assertEqual(('123', '456'), self.xadapter.rule_obfuscated_coordinates(test_record))

if __name__ == "__main__":
    unittest.main()