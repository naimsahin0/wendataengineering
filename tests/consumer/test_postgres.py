import unittest
from unittest.mock import MagicMock, patch
from package.service.consumer.postgres import PostgresConsumer

class TestPostgresConsumer(unittest.TestCase):

    def setUp(self):
        self.record = {
            "category": {
                "CategoryId": 1,
                "CategoryName": "Luxury"
            },
            "chain": {
                "ChainId": 1,
                "ChainName": "Marriott"
            },
            "hotel": {
                "HotelId": 1,
                "HotelName": "Ritz Carlton",
                "CategoryId": 1,
                "ChainId": 1,
                "Latitude": 123.12,
                "Longitude": 456.56
            }
        }
        self.mock_db = MagicMock()
        self.consumer = PostgresConsumer(None, None, self.mock_db)

    @patch('package.service.consumer.postgres.CustomError')
    def test_insert_data_success(self, mock_CustomError):
        cur = self.mock_db.instance().cursor().__enter__()

        self.consumer.insert_data(self.record)

        cur.execute.assert_any_call("""
                        INSERT INTO public.Category (CategoryId, CategoryName)
                        VALUES (%s, %s)
                        ON CONFLICT (CategoryId) DO UPDATE SET
                            CategoryName = EXCLUDED.CategoryName;
                        """, (self.record['category']['CategoryId'], self.record['category']['CategoryName']))

        cur.execute.assert_any_call("""
                        INSERT INTO public.Chain (ChainId, ChainName)
                        VALUES (%s, %s)
                        ON CONFLICT (ChainId) DO UPDATE SET
                            ChainName = EXCLUDED.ChainName;
                        """, (self.record['chain']['ChainId'], self.record['chain']['ChainName']))

        cur.execute.assert_any_call("""
                        INSERT INTO public.Hotel (HotelId, HotelName, CategoryId, ChainId, Latitude, Longitude)
                        VALUES (%s, %s, %s, %s, %s, %s)
                        ON CONFLICT (HotelId) DO UPDATE SET
                            HotelName = EXCLUDED.HotelName,
                            CategoryId = EXCLUDED.CategoryId,
                            ChainId = EXCLUDED.ChainId,
                            Latitude = EXCLUDED.Latitude,
                            Longitude = EXCLUDED.Longitude;
                        """, (self.record['hotel']['HotelId'], self.record['hotel']['HotelName'], self.record['hotel']['CategoryId'], self.record['hotel']['ChainId'],
                              self.record['hotel']['Latitude'], self.record['hotel']['Longitude']))

        self.mock_db.instance().commit.assert_called_once()
        self.mock_db.instance().close.assert_called_once()
        mock_CustomError.assert_not_called()

    @patch('package.service.consumer.postgres.CustomError')
    def test_insert_data_failure(self, mock_CustomError):
        cur = self.mock_db.instance().cursor().__enter__()
        cur.execute.side_effect = Exception

        with self.assertRaises(Exception):
            self.consumer.insert_data(self.record)

        self.mock_db.instance().rollback.assert_called_once()
        self.mock_db.instance().close.assert_called_once()
        mock_CustomError.assert_called_once()

if __name__ == "__main__":
    unittest.main()
