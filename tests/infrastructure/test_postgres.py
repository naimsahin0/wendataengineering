import unittest
from unittest.mock import patch
from package.infrastructure import postgres

class TestPostgresDatabase(unittest.TestCase):
    """
    Test cases for the PostgresDatabase class in the postgres module.
    """
    
    @patch('psycopg2.connect')
    def test_instance(self, mock_psycopg2_connect):
        """
        Test to verify that a psycopg2 connection is established when instance method is called
        """
        db_params = {
            'dbname': 'test_db',
            'user': 'test_user',
            'password': 'test_pass',
            'host': 'localhost',
            'port': 5432
        }
        db = postgres.PostgresDatabase(db_params)
        db.instance()
        mock_psycopg2_connect.assert_called_once_with(**db_params)


if __name__ == "__main__":
    unittest.main()