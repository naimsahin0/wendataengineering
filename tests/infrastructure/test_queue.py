import unittest
import queue
from typing import Any
from package.infrastructure import queue as q


class TestCustomQueue(unittest.TestCase):
    def setUp(self):
        self.custom_queue = q.CustomQueue()

    def test_enqueue(self):
        self.custom_queue.enqueue(5)
        self.assertEqual(self.custom_queue.size(), 1)

    def test_dequeue(self):
        self.custom_queue.enqueue(5)
        self.assertEqual(self.custom_queue.dequeue(), 5)

    def test_is_empty_when_empty(self):
        self.assertEqual(self.custom_queue.is_empty(), True)

    def test_is_empty_when_not_empty(self):
        self.custom_queue.enqueue(5)
        self.assertEqual(self.custom_queue.is_empty(), False)

    def test_size_when_empty(self):
        self.assertEqual(self.custom_queue.size(), 0)

    def test_size_when_not_empty(self):
        self.custom_queue.enqueue(5)
        self.assertEqual(self.custom_queue.size(), 1)

    def tearDown(self):
        while not self.custom_queue.is_empty():
            self.custom_queue.dequeue()


if __name__ == '__main__':
    unittest.main()