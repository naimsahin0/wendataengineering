import unittest
from package.validate.X import XValidator, CustomError, XHotelModel
from pydantic import ValidationError

class TestXValidator(unittest.TestCase):
    def setUp(self):
        self.validator = XValidator()

    def test_check_valid_record(self):
        valid_record = {
            "property_id": 100,
            "name": "Test Resort",
            "category": {"id": 1, "name": "Resort"},
            "chain": {"id": 1, "name": "Luxury Resort Chain"},
            "location": {
                "coordinates": {"latitude": 33.3333, "longitude": 44.4444},
                "obfuscation_required": True,
                "obfuscated_coordinates": {"latitude": 33.3344, "longitude": 44.4455}
            }
        }
        try:
            self.validator.check(valid_record)
        except CustomError:
            self.fail("check() method raised CustomError unexpectedly!")

    def test_check_invalid_record(self):
        invalid_record = {
            "property_id": "one hundred",  # Invalid type: string instead of int
            "name": "Test Villa",
            "category": {"id": "3", "name": "Villa"},  # id should be an integer
            "chain": {"id": 3, "name": "Premium Villa Chains"},
            "location": {
                "coordinates": {"latitude": "30.3030", "longitude": "-40.4040"},  # Latitude and longitude should be floats
                "obfuscated_coordinates": {"latitude": 30.3040, "longitude": -40.4050},
                "obfuscation_required": False
            }
        }
        with self.assertRaises(CustomError):
            self.validator.check(invalid_record)

    def test_check_data_structure_invalid_record(self):
        invalid_record = {
            "coordinates": {"latitude": 33.3333, "longitude": 44.4444},
            "obfuscation_required": True
        }
        with self.assertRaises(ValidationError):
            self.validator.check_data_structure(invalid_record)


if __name__ == '__main__':
    unittest.main()