import json
import queue
import unittest
from unittest.mock import Mock, patch
from package.provider.X import XProvider
from package.validate.exception import CustomError
from package.infrastructure.queue import CustomQueue


class XProviderTests(unittest.TestCase):
    @patch('builtins.open', new_callable=unittest.mock.mock_open, read_data='{"key": "value"}')
    def test_provide_data(self, mock_open):
        test_queue = CustomQueue()
        test_filepath = 'test_file.json'
        test_adapter = Mock()
        test_validator = Mock()
        test_adapter.adapt = Mock(return_value='adapted_record')
        test_validator.check = Mock(return_value=True)

        test_provider = XProvider(test_filepath, test_queue, test_queue, test_adapter, test_validator)
        test_provider.run()

        mock_open.assert_called_once_with(test_filepath, 'r')
        self.assertEqual(test_queue.size(), 1)
        self.assertEqual(test_queue.dequeue(), 'adapted_record')

    @patch('builtins.open', new_callable=unittest.mock.mock_open, read_data='{"key": "bad_value"}')
    def test_provide_data_with_customerror(self, mock_open):
        test_queue = CustomQueue()
        test_filepath = 'test_file.json'
        test_adapter = Mock()
        test_validator = Mock()
        test_adapter.adapt = Mock(side_effect=CustomError('error','','',''))
        test_validator.check = Mock(return_value=True)

        test_provider = XProvider(test_filepath, test_queue, test_queue, test_adapter, test_validator)
        test_provider.run()

        mock_open.assert_called_once_with(test_filepath, 'r')
        self.assertEqual(test_queue.size(), 1)
        test_error = test_queue.dequeue()
        self.assertEqual(test_error['message'], 'error')

    @patch('builtins.open', new_callable=unittest.mock.mock_open, read_data='{"key": "bad_value"}')
    def test_provide_data_with_exception(self, mock_open):
        test_queue = CustomQueue()
        test_filepath = 'test_file.json'
        test_adapter = Mock()
        test_validator = Mock()
        test_adapter.adapt = Mock(side_effect=Exception('error'))
        test_validator.check = Mock(return_value=True)

        test_provider = XProvider(test_filepath, test_queue, test_queue, test_adapter, test_validator)
        test_provider.run()

        mock_open.assert_called_once_with(test_filepath, 'r')
        self.assertEqual(test_queue.size(), 1)
        test_exception = test_queue.dequeue()
        self.assertEqual(test_exception['message'], 'error')


if __name__ == '__main__':
    unittest.main()
